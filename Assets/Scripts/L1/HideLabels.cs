using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideLabels : MonoBehaviour
{
    public bool differentLabels;
   
    public Toggle labelToggle;
    public GameObject BoardPinToggleButton;

    public bool LabelOnOffToggleButton;
    public bool boardToggleStateBool;

    public GameObject BoardLabels;
    public GameObject PinLabels;
    
    public GameObject Labels;

    // Start is called before the first frame update
    void Start()
    {
      
        labelToggle.GetComponent<Toggle>();
        BoardPinToggleButton.GetComponent<Toggle>();
        
       // labelToggleState = false;
       // boardToggleState = false;
      //  BoardLabels.SetActive(false);
      //  PinLabels.SetActive(false);
       // boardToggle.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //  OnLabelClicked();
        if (this.gameObject.name == "Sugar_Cube_Relay_5V" || this.gameObject.name == "Gear_DC_Motor" || this.gameObject.name == "SG90 _Motor" || this.gameObject.name == "MG995_Servo_motor")
        {
            BoardPinToggleButton.SetActive(false);
        }
        



    }
    public void OnLabelClicked()
    {
        

            if (differentLabels)
        {
            Debug.Log(LabelOnOffToggleButton);
            if(labelToggle.isOn)
            {
                BoardPinToggleButton.SetActive(true);
                if (boardToggleStateBool == false)
                {
                    BoardLabels.SetActive(false);
                    PinLabels.SetActive(true);
                }
                else if (boardToggleStateBool == true)
                {
                    BoardLabels.SetActive(true);
                    PinLabels.SetActive(false);
                }
               
            }
            else if(!labelToggle.isOn)
            {
                BoardPinToggleButton.SetActive(false);
                BoardLabels.SetActive(false);
                PinLabels.SetActive(false);
                LabelOnOffToggleButton = true;
            }
            //if (LabelOnOffToggleButton == false)
            //{              
            //    BoardPinToggleButton.SetActive(false);             
            //    BoardLabels.SetActive(false);
            //    PinLabels.SetActive(false);
            //    LabelOnOffToggleButton = true;
            //}
            //else if (LabelOnOffToggleButton == true)
            //{
            //    BoardPinToggleButton.SetActive(true);
            //    if (boardToggleStateBool == true)
            //    {
            //        BoardLabels.SetActive(false);
            //        PinLabels.SetActive(true);
            //    }
            //    else if (boardToggleStateBool == false)
            //    {
            //        BoardLabels.SetActive(true);
            //        PinLabels.SetActive(false);
            //    }

            //    LabelOnOffToggleButton = false;
            //}
        }
        else if (differentLabels == false)
        {
            Debug.Log(this.gameObject.name);
             
            if (LabelOnOffToggleButton == false)
            {
                Labels.SetActive(true);
                LabelOnOffToggleButton = true;
            }
            else
            {
                Labels.SetActive(false);
                LabelOnOffToggleButton = false;
            }
        } 
            
    }

    public void OnBoardLabelClicked()
    {
        if (boardToggleStateBool == true)
        {
            BoardLabels.SetActive(false);
            PinLabels.SetActive(true);
            boardToggleStateBool = false;
        }
        else
        {
            BoardLabels.SetActive(true);
            PinLabels.SetActive(false);
            boardToggleStateBool = true;
        }
    }

    //public void OnLabelClicked()
    //{
    //    if (differentLabels)
    //    {
    //        Debug.Log(LabelOnOffToggleButton);
    //        if (LabelOnOffToggleButton==false)
    //        {
    //            BoardPinToggleButton.SetActive(false);
    //            //  BoardPinToggleButton.SetActive(true);
    //            BoardLabels.SetActive(false);
    //            PinLabels.SetActive(false);
    //           // Labels.SetActive(false);
    //            LabelOnOffToggleButton = true;
    //        }
    //        else if(LabelOnOffToggleButton==true)
    //        {
    //            BoardPinToggleButton.SetActive(true);
    //            LabelOnOffToggleButton = false;
    //        }
    //    }
    //    else if (differentLabels==false)
    //    {
    //        if (LabelOnOffToggleButton==false)
    //        {
    //            Labels.SetActive(true);
    //            LabelOnOffToggleButton = true;
    //        }
    //        else
    //        {
    //            Labels.SetActive(false);
    //            LabelOnOffToggleButton = false;
    //        }
    //    }



    //}

    //public void OnBoardLabelClicked()
    //{
    //    if (boardToggleState==true)
    //    {
    //        BoardLabels.SetActive(false);
    //        PinLabels.SetActive(true);
    //        boardToggleState = false;
    //    }
    //    else
    //    {
    //        BoardLabels.SetActive(true);
    //        PinLabels.SetActive(false);
    //        boardToggleState=true;
    //    }
    //}

}