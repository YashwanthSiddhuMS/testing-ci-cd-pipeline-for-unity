using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtons : MonoBehaviour
{
    public GameObject Reset;
    public GameObject Label;
    public GameObject Exit;
    public GameObject ExitPanel;
   // public GameObject ExitYes;
  //  public GameObject ExitNo;
    public GameObject YesButton;
    public GameObject NoButton;
    public GameObject HomeButton;
    bool active=false;
    bool arrowbutton = false;
    public GameObject nextButton;
    public GameObject previousButton;
    public GameObject menuButton;
    public GameObject BoardPinLabel;

    //new
    public Sprite beforeClick;
    public Sprite onClick;

    // Start is called before the first frame update
    void Start()
    {
        ExitPanel.SetActive(false);
        YesButton.SetActive(false);
        NoButton.SetActive(false);
        //Reset.SetActive(false);
        //Exit.SetActive(false);
        //Label.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void menuOptionClicked()
    {
        if (active == false)
        {
            //new
            menuButton.GetComponent<Image>().sprite = onClick;

            Reset.SetActive(true);
            Label.SetActive(true);
            Exit.SetActive(true);
            HomeButton.SetActive(true);
            active = true;
        }
        else
        {
            //new
            menuButton.GetComponent<Image>().sprite = beforeClick;

            Reset.SetActive(false);
            Label.SetActive(false);
            Exit.SetActive(false);
            HomeButton.SetActive(false);
            active = false;

        }
    }
    public void ExitPanelActive()
    {
        ExitPanel.SetActive(true);
        YesButton.SetActive(true);
        NoButton.SetActive(true);
        Reset.SetActive(false);
        Label.SetActive(false);
        BoardPinLabel.SetActive(false);
        Exit.SetActive(false);
        nextButton.SetActive(false);
        previousButton.SetActive(false);
        HomeButton.SetActive(false);

    }
    public void Exityes()
    {
        Application.Quit();
    }
    public void Exitno()
    {
        ExitPanel.SetActive(false);
        YesButton.SetActive(false);
        NoButton.SetActive(false);
        nextButton.SetActive(true);
        previousButton.SetActive(true);
    }
    public void arrowbuttons()
    {
        if (arrowbutton == false)
        {
            nextButton.SetActive(true);
            previousButton.SetActive(true);
            menuButton.SetActive(true);
            menuButton.GetComponent<Image>().sprite = beforeClick;
            
            arrowbutton = true;
            

        }
        else
        {
            
            nextButton.SetActive(false);
            previousButton.SetActive(false);
            menuButton.SetActive(false);
            arrowbutton = false;
            Reset.SetActive(false);
            Label.SetActive(false);
            HomeButton.SetActive(false);
            
            Exit.SetActive(false);

        }
    }
}
