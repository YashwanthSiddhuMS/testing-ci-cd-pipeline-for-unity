using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HomeController : MonoBehaviour
{
    public GameObject initialScene;
    public List<GameObject> prefab;
    public GameObject exitPanel;
   
   
   
    public void Sensors()
    {
        disabledAllObjects();
        prefab[0].SetActive(true);
    }
    
    public void Controllers()
    {
        disabledAllObjects();
        prefab[1].SetActive(true);
    }

    public void Mechanical()
    {
        disabledAllObjects();
        prefab[2].SetActive(true);
    }

    public void Electric()
    {
        disabledAllObjects();
        prefab[3].SetActive(true);
    }

    public void Home()
    {
        disabledAllObjects();
        initialScene.SetActive(true);
    }

    public void ExitButtonPressed()
    {
        exitPanel.SetActive(true);
    }

    public void NoButtonPressed()
    {
        exitPanel.SetActive(false);
    }

    public void YesButtonPressed()
    {
        Application.Quit();
    }



    public void disabledAllObjects()
    {
        initialScene.SetActive(false);
        for(int i = 0; i < prefab.Count; i++)
        {
            prefab[i].SetActive(false);
        }
    }
    
}
