using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
 
public class Nav : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
 
    // Update is called once per frame
    void Update()
    {
 
    }
 
    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        if (sceneName.Contains("ARScene"));
        {
            GlobalVariable.directedFromScene = "ARScene";
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadPreviousScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void GoToScene(string sceneName)
    {
        StartCoroutine(DelayGoToScene(sceneName));
    }

    public IEnumerator DelayGoToScene(string sceneName)
    {
        yield return new WaitForSeconds(0.1f);
        StopAllCoroutines();
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        Application.Quit();
    }


}