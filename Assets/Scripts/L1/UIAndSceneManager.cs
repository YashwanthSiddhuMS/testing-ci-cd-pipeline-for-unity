using Lean.Common;
using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class UIAndSceneManager : MonoBehaviour
{
    #region Main Scene Required Declaration
    [Header("Main Scene")]
    [SerializeField]
    GameObject mainPanel;
    [SerializeField]
    GameObject settingsPanel;
    [SerializeField]
    GameObject exitPanel;
    #endregion

    #region Experiment Scene Required Declaration
    [Header("Experiment Scene")]
    [SerializeField]
    GameObject _3DViewerObject;
    [SerializeField]
    GameObject theoryPanel;
    [SerializeField]
    GameObject noteBookPanel;
    [SerializeField]
    GameObject instructionPanel;

    bool stateOfInstructionPanel;

    [SerializeField]
    ARSession arSession;
    [SerializeField]
    GameObject arSessionOrigin;
    [SerializeField]
    GameObject arCamera;
    [SerializeField]
    GameObject arScreenSpaceUI;
    #endregion

    #region 3D Viewer Required Declaration
    [Header("3D Viewer")]
    [SerializeField]
    GameObject ExperimentObject;
    [SerializeField]
    GameObject closeButtonText;
    [SerializeField]
    GameObject continueButtonText;
    [SerializeField]
    ChangeObject materialsTag;
    [SerializeField]
    ChangeObject laboratoryObjects;
    [SerializeField]
    GameObject slideMenuPanel;
    [SerializeField]
    GameObject modelObjects;

    public bool toggleTo3D = false;
    public int sceneType = 1;

    [SerializeField]
    GameObject screenSpaceController, worldSpaceController;
    [SerializeField]
    GameObject skyBoxObject;
    [SerializeField]
    GameObject cameraPivot;

    Animator slideMenuPanelAnimator;

    Vector3 defaultARPosition = new Vector3(0f, 0.75f, 0f), defaultARRotation = new Vector3(0f, 0f, 0f), defaultARScale = new Vector3(1f, 1f, 1f);
    Vector3 default3DPosition = new Vector3(0f, 0f, 0f), default3DRotation = new Vector3(0f, 0f, 0f), default3DScale = new Vector3(1f, 1f, 1f);
    Vector3 defaultCameraPosition = new Vector3(0f, 0.1f, 0f), defaultCameraRotation = new Vector3(45f, 45f, 0f);
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        if (arSession != null)
            arSession = arSession.GetComponent<ARSession>();

        if (!string.IsNullOrEmpty(GlobalVariable.directedFromScene) && GlobalVariable.directedFromScene.Equals("ARScene"))
        {
            closeButtonText.SetActive(true);
            continueButtonText.SetActive(false);
        }
        if (!string.IsNullOrEmpty(GlobalVariable.directedFromScene) && GlobalVariable.directedFromScene.Equals("Main Scene"))
        {
            closeButtonText.SetActive(false);
            continueButtonText.SetActive(true);
        }

        if (slideMenuPanel != null)
            slideMenuPanelAnimator = slideMenuPanel.GetComponent<Animator>();

        materialsTag = materialsTag.GetComponent<ChangeObject>();
        laboratoryObjects = laboratoryObjects.GetComponent<ChangeObject>();

    }

    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Main Scene
    public void OnSettingsButtonClicked()
    {

    }

    public void OnExitButtonClicked()
    {
        Application.Quit();
    }
    #endregion

    #region Experiment Scene
    public void On3DViewerButtonClicked(string sceneName)
    {
        GlobalVariable.Clicked3DViewer(sceneName);

        if (!string.IsNullOrEmpty(GlobalVariable.directedFromScene) && GlobalVariable.directedFromScene.Equals("ARScene"))
        {
            closeButtonText.SetActive(true);
            continueButtonText.SetActive(false);
        }

        if (toggleTo3D)
        {
            OnAR3DToggleButtonClicked();
        }
        else
        {
            OnARResetButtonClicked();
        }

        _3DViewerObject.SetActive(true);
        ExperimentObject.SetActive(false);
    }

    public void OnTheoryButtonClicked()
    {
        theoryPanel.SetActive(!theoryPanel.activeSelf);

        if(theoryPanel.activeSelf)
        {
            stateOfInstructionPanel = instructionPanel.activeSelf;
            instructionPanel.SetActive(false);
        }
        else
        {
            instructionPanel.SetActive(stateOfInstructionPanel);
        }
    }

    public void closeTheoryPanel()
    {
        theoryPanel.SetActive(false);
        instructionPanel.SetActive(stateOfInstructionPanel);
    }

    public void OnNoteBookButtonClicked()
    {
        noteBookPanel.SetActive(!noteBookPanel.activeSelf);
    }

    public void OnInstructionButtonClicked()
    {
        instructionPanel.SetActive(!instructionPanel.activeSelf);
    }
    #endregion

    #region 3D Viewer
    public void OnSlideButtonClicked()
    {
        if(slideMenuPanel != null && slideMenuPanelAnimator != null)
        {
            slideMenuPanelAnimator.SetBool("ShowMenu", !slideMenuPanelAnimator.GetBool("ShowMenu"));
        }
    }

    public void OnARResetButtonClicked()
    {
        // Debug.Log("In AR");
        modelObjects.transform.localPosition = defaultARPosition;
        modelObjects.transform.localEulerAngles = defaultARRotation;
        modelObjects.transform.localScale = defaultARScale;
        
        for (int i = 0; i < modelObjects.transform.childCount; i++)
        {
            modelObjects.transform.GetChild(i).gameObject.SetActive(i == 0);
            /*
            modelObjects.transform.GetChild(i).localPosition.Set(0f, 0f, 0f);
            modelObjects.transform.GetChild(i).localEulerAngles.Set(0f, 0f, 0f);
            modelObjects.transform.GetChild(i).localScale.Set(0f, 0f, 0f);
            */
        }

        // modelObjects.GetComponent<LeanSelectableByFinger>().enabled = true;
        // modelObjects.GetComponent<LeanManualTranslate>().enabled = true;
        modelObjects.GetComponent<LeanManualRotate>().enabled = true;
        modelObjects.GetComponent<LeanMultiUpdate>().enabled = true;
        // modelObjects.GetComponent<LeanTwistRotateAxis>().enabled = true;
        modelObjects.GetComponent<LeanPinchScale>().enabled = true;

        materialsTag.ChangeObjects(0);
        laboratoryObjects.ChangeObjects(0);
    }

    public void On3DResetButtonClicked()
    {
        // Debug.Log("In Orbital 3D");
        modelObjects.transform.localPosition = default3DPosition;
        modelObjects.transform.localEulerAngles = default3DRotation;
        modelObjects.transform.localScale = default3DScale;

        cameraPivot.transform.localPosition = defaultCameraPosition;
        cameraPivot.transform.localEulerAngles = defaultCameraRotation;
        cameraPivot.transform.GetChild(0).GetComponent<Camera>().fieldOfView = 30f;
        
        for (int i = 0; i < modelObjects.transform.childCount; i++)
        {
            modelObjects.transform.GetChild(i).gameObject.SetActive(i == 0);
            /*
            modelObjects.transform.GetChild(i).localPosition.Set(0f, 0f, 0f);
            modelObjects.transform.GetChild(i).localEulerAngles.Set(0f, 0f, 0f);
            modelObjects.transform.GetChild(i).localScale.Set(0f, 0f, 0f);
            */
        }

        // modelObjects.GetComponent<LeanSelectableByFinger>().enabled = false;
        // modelObjects.GetComponent<LeanManualTranslate>().enabled = false;
        modelObjects.GetComponent<LeanManualRotate>().enabled = false;
        modelObjects.GetComponent<LeanMultiUpdate>().enabled = false;
        // modelObjects.GetComponent<LeanTwistRotateAxis>().enabled = false;
        modelObjects.GetComponent<LeanPinchScale>().enabled = false;

        materialsTag.ChangeObjects(0);
        laboratoryObjects.ChangeObjects(0);
    }

    public void OnAR3DToggleButtonClicked()
    {
        toggleTo3D = !toggleTo3D;
        if (toggleTo3D)
        {
            sceneType = 0;
            arSession.enabled = false;
            arScreenSpaceUI.SetActive(false);

            screenSpaceController.SetActive(true);
            worldSpaceController.SetActive(false);

            skyBoxObject.SetActive(true);
            cameraPivot.SetActive(true);

            On3DResetButtonClicked();
        }
        else
        {
            sceneType = 1;
            arSession.enabled = true;
            arScreenSpaceUI.SetActive(true);

            screenSpaceController.SetActive(false);
            worldSpaceController.SetActive(true);

            skyBoxObject.SetActive(false);
            cameraPivot.SetActive(false);

            OnARResetButtonClicked();
        }
    }

    public void OnCloseButtonClicked(Toggle toggle)
    {
        if (toggle.isOn)
        {
            toggle.isOn = false;
            // OnAR3DToggleButtonClicked();
        }

        ExperimentObject.SetActive(true);
        _3DViewerObject.SetActive(false);
    }
    #endregion


}
