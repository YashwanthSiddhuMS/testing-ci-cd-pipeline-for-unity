using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtNewScript : MonoBehaviour
{
    public Quaternion DefaultRotation { get; private set; }

    Vector3 up;
    Vector3 forward;

    public enum PivotAxis
    {
        Free,
        X,
        Y
    }
    public PivotAxis PivotAxiss = PivotAxis.Free;
    private void Awake()
    {
        DefaultRotation = gameObject.transform.rotation;
    }
// Update is called once per frame
void Update()
    {
    switch (PivotAxiss)
    {
        case PivotAxis.X:
            Vector3 right = transform.right; // Fixed right
            forward = Vector3.ProjectOnPlane(Camera.main.transform.forward, right).normalized;
            up = Vector3.Cross(forward, right);
            break;
        case PivotAxis.Y:
            up = transform.up;
            forward = Vector3.ProjectOnPlane(Camera.main.transform.forward, up).normalized;
            break;
        case PivotAxis.Free:
        default:
            forward = Camera.main.transform.forward;
            up = Camera.main.transform.up;
            break;
    }
    transform.rotation = Quaternion.LookRotation(forward, up);

}
}
