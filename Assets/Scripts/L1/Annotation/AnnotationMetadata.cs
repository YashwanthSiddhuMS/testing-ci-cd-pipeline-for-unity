﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnotationMetadata : MonoBehaviour
{
    public string anatomicalName, classification, location, description, partid;



    public string GetInfotipText()
    {
        if (classification == "" && location == "") return description;

        return "Classification: " + classification + "\n\nLocation: " + location + "\n\nDescription: " + description;
    }

    public string GetInfotipTitle()
    {
        return anatomicalName;
    }
}
