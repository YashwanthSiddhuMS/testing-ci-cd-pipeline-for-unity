//dev@holosuit.com
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class AnnotationHandler : MonoBehaviour
{
   
    public static bool IsMouseBeingDragged = false;
    public float CanvasRotationDampening = 5.0f;
    public bool Toggle;
    private GameObject ExaminCam;
    private static Vector3 lastMousePosition;
    private static float stayingStillSince;
    private static bool mouseWasBeingDragged = false;
    [SerializeField]
    private Transform[] annotationCanvasList;

    private void Start()
    {
        //Getting list of all child object 
        annotationCanvasList = GetComponentsInChildren<Transform>(true);
        //ExaminCam = GameObject.FindGameObjectWithTag("OrbitalCamera");
        ExaminCam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        //Hide and show Text on mouse click need to implement.
        //if(Input.GetMouseButtonDown(0))
        //{
        //    Toggle = !Toggle;
        //    ToggleAnnotation(Toggle);
        //}
        //Rotate annotation on dragging mouse.
        if(!CheckIfMouseBeingDragged())
        {
            RotateAnnotations();
        }
    }

    //Toggle annotation on click
    private void ToggleAnnotation(bool toggle)
    {
        foreach (Transform annotation in annotationCanvasList.Skip(1))
        {
            if (annotation.tag == "Annotation")
            {
                annotation.gameObject.SetActive(toggle);  
            }
        }
    }

    //Mouse is not being dragged rotate the text canvas.
    private void RotateAnnotations()
    {
       
        foreach (Transform annotationCanvas in annotationCanvasList.Skip(1))
        {
            if (annotationCanvas.tag == "AnnotationCanvas")
            {
                Quaternion desiredRotation = Quaternion.LookRotation(ExaminCam.transform.forward);
                Quaternion rotation = Quaternion.Lerp(annotationCanvas.rotation, desiredRotation, Time.deltaTime * CanvasRotationDampening);
                annotationCanvas.rotation = rotation;
            }
        }
    }
   
    //Check if mouse is being Dragged or not
    public bool CheckIfMouseBeingDragged()
    {
        if (Input.GetMouseButtonDown(1))
        {
            mouseWasBeingDragged = false;
        }
        if (Input.GetMouseButton(1))
        {
            float deltaMousePos = (Input.mousePosition - lastMousePosition).magnitude;
            lastMousePosition = Input.mousePosition;
            if (deltaMousePos < 100)
            {
                if (Time.realtimeSinceStartup - stayingStillSince > 0.2f)
                {
                    return IsMouseBeingDragged = false || mouseWasBeingDragged;
                }
                else
                    return IsMouseBeingDragged = true;
            }
            else
            {
                stayingStillSince = Time.realtimeSinceStartup;
                mouseWasBeingDragged = true;
                return IsMouseBeingDragged = true;
            }
        }
        else
        {
            return IsMouseBeingDragged = false;
        }
    }

}
