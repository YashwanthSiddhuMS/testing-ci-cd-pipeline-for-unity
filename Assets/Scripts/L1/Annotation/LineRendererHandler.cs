using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererHandler : MonoBehaviour
{
    public Transform StarPosition;
    public Transform DestinationPosition;
   
    private LineRenderer annotationLine;
    private void Start()
    {
        annotationLine = GetComponent<LineRenderer>();
        SetLineRenderer();
    }

    private void Update()
    {
        SetLineRenderer();
    }
    private void SetLineRenderer()
    {
        annotationLine.SetPosition(0,StarPosition.position);
        annotationLine.SetPosition(1,DestinationPosition.position);
    }
}
