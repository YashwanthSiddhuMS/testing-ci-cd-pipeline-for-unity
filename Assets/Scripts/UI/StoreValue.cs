using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StoreValue : MonoBehaviour
{

    public TMP_InputField inputField;
    public TextMeshProUGUI inputText;
    public string Name = "userName";
    // Start is called before the first frame update
    void Start()
    {
        inputField.gameObject.SetActive(false);
        inputField.text = "";
        inputText.text = PlayerPrefs.GetString(Name);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StoreName()
    {
        //Name = inputField.text;
        inputText.text = inputField.text;
        PlayerPrefs.SetString(Name, inputText.text);
        PlayerPrefs.Save();
        inputText.gameObject.SetActive(true);
        inputField.gameObject.SetActive(false);
    }

    public void enableObject()
    {
        inputField.gameObject.SetActive(true);
    }
}
