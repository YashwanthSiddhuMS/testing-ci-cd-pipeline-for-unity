using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class SignUp : MonoBehaviour
{
    private const string PASSWORD_REGEX = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,24})";

    [SerializeField] private string createEndpoint = "http://localhost:13756/account/create";

   //SerializeField] private TextMeshProUGUI alertText;
    [SerializeField] private TextMeshProUGUI Successfull;
    [SerializeField] private Button createButton;
    [SerializeField] private TMP_InputField usernameFirstName;
    [SerializeField] private TMP_InputField usernameLastName;
    [SerializeField] private TMP_InputField phoneNumber;
    [SerializeField] private TMP_Dropdown Gender;
    [SerializeField] private TMP_InputField DOB;
    [SerializeField] private TMP_Dropdown Country;
    [SerializeField] private TMP_Dropdown State;
    [SerializeField] private TMP_Dropdown City;
    [SerializeField] private TMP_InputField Zipcode;
    [SerializeField] private TMP_InputField Email;  
    [SerializeField] private TMP_InputField passwordInputField;
    [SerializeField] private TMP_InputField confirm_passwordInputField;
    [SerializeField] private TextMeshProUGUI Invalid_EmailId;
    public void OnCreateClick()
    {
      //alertText.text = "Creating account...";
        ActivateButtons(false);
        StartCoroutine(TryCreate());
    }
    private IEnumerator TryCreate()
    {
        string usernameF = usernameFirstName.text;
        string usernameL=usernameLastName.text;
        string password = passwordInputField.text;
        string confirm_password = confirm_passwordInputField.text;
        string phonenumber = phoneNumber.text;
        string gender = Gender.options[Gender.value].text;
        string dob = DOB.text;
        string country=Country.options[Country.value].text;
        string state=State.options[State.value].text;
        string city=City.options[City.value].text;
        string zipcode = Zipcode.text;
        string EmailID = Email.text;
        if (usernameF.Length < 3 || usernameF.Length > 24)
        {
            //alertText.text = "Invalid username";
          //Invalid_EmailId.gameObject.SetActive(true);
            ActivateButtons(true);
            yield break;
        }

        if (!Regex.IsMatch(password, PASSWORD_REGEX))
        {
        //  alertText.text = "Invalid credentials";
            ActivateButtons(true);
            yield break;
        }

        WWWForm form = new WWWForm();
        form.AddField("rUsername", usernameF);
        form.AddField("rUsernameL", usernameL);
        form.AddField("rPassword", password);
        form.AddField("confirm_Password", confirm_password);
        form.AddField("phonenumber",phonenumber);
        form.AddField("gender",gender);
        form.AddField("dob",dob);
        form.AddField("state",state);
        form.AddField("country",country);
        form.AddField("city",city);
        form.AddField("zipcode",zipcode);
        form.AddField("rEmailID", EmailID);


        UnityWebRequest request = UnityWebRequest.Post(createEndpoint, form);
        var handler = request.SendWebRequest();

        float startTime = 0.0f;
        while (!handler.isDone)
        {
            startTime += Time.deltaTime;

            if (startTime > 10.0f)
            {
                break;
            }

            yield return null;
        }

        if (request.result == UnityWebRequest.Result.Success)
        {
            Debug.Log(request.downloadHandler.text);
            CreateResponse response = JsonUtility.FromJson<CreateResponse>(request.downloadHandler.text);

            if (response.code == 0)
            {
            //  alertText.text = "Account has been created!";
            Successfull.gameObject.SetActive(true);
            }
            else
            {
                switch (response.code)
                {
                    case 1:
               //       alertText.text = "Invalid credentials";
                        break;
                    case 2:
                    //  alertText.text = "Username is already taken";
                        break;
                    case 3:
                    //  alertText.text = "Password is unsafe";
                        break;
                    default:
                    //  alertText.text = "Corruption detected";
                        break;

                }
            }
        }
        else
        {
          //alertText.text = "Error connecting to the server...";
        }

        ActivateButtons(true);

        yield return null;
    }

    private void ActivateButtons(bool toggle)
    { 
        createButton.interactable = toggle;
    }
}
