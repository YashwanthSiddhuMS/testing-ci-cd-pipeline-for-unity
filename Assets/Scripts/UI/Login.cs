using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class Login : MonoBehaviour
{
    private const string PASSWORD_REGEX = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,24})";

    [SerializeField] private string loginEndpoint = "http://localhost:13756/account/login";

  //[SerializeField] private TextMeshProUGUI alertText;
    [SerializeField] private Button loginButton;
    [SerializeField] private TMP_InputField emailInputField;
    [SerializeField] private TMP_InputField passwordInputField;
    [SerializeField] private TextMeshProUGUI Invalid_EmailId;
    [SerializeField] private TextMeshProUGUI Invalid_Password;
    [SerializeField] private TextMeshProUGUI Successfull;
    [SerializeField] private TextMeshProUGUI AccountNotActivated;
    public void OnLoginClick()
    {
      //alertText.text = "Signing in...";
        ActivateButtons(false);
        Invalid_EmailId.gameObject.SetActive(false);
        Invalid_Password.gameObject.SetActive(false);
        AccountNotActivated.gameObject.SetActive(false);
        StartCoroutine(TryLogin());
    }

    private IEnumerator TryLogin()
    {   
        string EmailID = emailInputField.text;
        string password = passwordInputField.text;
        //Debug.Log($"{username}:{password}");
        /*        if (emailID.Length < 10 || emailID.Length > 24)
                {
                    alertText.text = "Invalid emailID";
                    Invalid_EmailId.gameObject.SetActive(true);
                    ActivateButtons(true);
                    yield break;
                }*/

/*        if (!Regex.IsMatch(password, PASSWORD_REGEX))
        {
            alertText.text = "Invalid credentials";
            Invalid_Password.gameObject.SetActive(true);
            ActivateButtons(true);
            yield break;
        }*/

        WWWForm form = new WWWForm();
        form.AddField("rEmailID", EmailID);
        form.AddField("rPassword", password);

        UnityWebRequest request = UnityWebRequest.Post(loginEndpoint, form);
        var handler = request.SendWebRequest();

        float startTime = 0.0f;
        while (!handler.isDone)
        {
            startTime += Time.deltaTime;

            if (startTime > 10.0f)
            {
                break;
            }

            yield return null;
        }

        if (request.result == UnityWebRequest.Result.Success)
        {
            LoginResponse response = JsonUtility.FromJson<LoginResponse>(request.downloadHandler.text);

            if (response.code == 0) // login success?
            {
                ActivateButtons(false);
                Successfull.gameObject.SetActive(true);
             // alertText.text = "Welcome " + ((response.data.adminFlag == 1) ? " Admin" : "");
            }
            else
            {
                switch (response.code)
                {
                    case 1:
                        Invalid_Password.gameObject.SetActive(true);
                        ActivateButtons(true);
                        break;
                    case 2:

                       Invalid_EmailId.gameObject.SetActive(true);
                       ActivateButtons(true);
                        break;
                    case 3:
                       AccountNotActivated.gameObject.SetActive(true);
                        ActivateButtons(true);
                        break;
                    default:
                      //alertText.text = "Corruption detected";
                        ActivateButtons(false);
                        break;
                }
            }
        }
        else
        {
         // alertText.text = "Error connecting to the server...";
            ActivateButtons(true);
        }


        yield return null;
    }

    
    private void ActivateButtons(bool toggle)
    {
        loginButton.interactable = toggle;
    }
}
