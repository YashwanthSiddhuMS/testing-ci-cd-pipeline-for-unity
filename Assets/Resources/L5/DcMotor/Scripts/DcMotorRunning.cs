using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DcMotorRunning : MonoBehaviour
{
    [SerializeField] GameObject wheel;
    public int clockwise = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        wheel.transform.Rotate(Vector3.forward, (int)Scale(5) * Time.deltaTime * clockwise);

    }
    double Scale(double val)
    {
        return (val / 12) * 180;
    }
}
