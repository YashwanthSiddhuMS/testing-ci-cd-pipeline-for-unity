using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Common;


public class MotorDriverCircut : MonoBehaviour
{
    [SerializeField] GameObject Battery;
    [SerializeField] GameObject leftMotor;
    [SerializeField] GameObject rightMotor;
    [SerializeField]  GameObject[] joints;
    [SerializeField] GameObject[] wires;
    [SerializeField] Vector3[] spawnAxis;
    [SerializeField] GameObject[] componentTerminals;
    public static Transform[] jointsTransform;
    int offsetspawn = 20;


    public static int jointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        jointsTransform = new Transform[joints.Length];
        for (int i = 0; i < joints.Length; i++)
        {
            jointsTransform[i] = joints[i].transform;
            joints[i].transform.position= new Vector3(joints[i].transform.position.x + (spawnAxis[i].x / offsetspawn), joints[i].transform.position.y + (spawnAxis[i].y / offsetspawn), joints[i].transform.position.z + (spawnAxis[i].z / offsetspawn));
            
            componentTerminals[i].SetActive(false);

            joints[i].AddComponent<LeanConstrainLocalPosition>();
            joints[i].GetComponent<LeanConstrainLocalPosition>().X = true;
            joints[i].GetComponent<LeanConstrainLocalPosition>().XMin = joints[i].transform.position.x;
            joints[i].GetComponent<LeanConstrainLocalPosition>().XMax = joints[i].transform.position.x;
            if (spawnAxis[i].x > 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().XMin = joints[i].transform.position.x-1;
              
            }
            else if(spawnAxis[i].x < 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().XMax = joints[i].transform.position.x+1;
            }
            joints[i].GetComponent<LeanConstrainLocalPosition>().Y = true;
            joints[i].GetComponent<LeanConstrainLocalPosition>().YMin = joints[i].transform.position.y;
            joints[i].GetComponent<LeanConstrainLocalPosition>().YMax = joints[i].transform.position.y;
            if (spawnAxis[i].y > 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().YMin = joints[i].transform.position.y - 1;

            }
            else if (spawnAxis[i].x < 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().YMax = joints[i].transform.position.y + 1;
            }
            joints[i].GetComponent<LeanConstrainLocalPosition>().Z = true;
            joints[i].GetComponent<LeanConstrainLocalPosition>().ZMin = joints[i].transform.position.z;
            joints[i].GetComponent<LeanConstrainLocalPosition>().ZMax = joints[i].transform.position.z;
            if (spawnAxis[i].z > 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().ZMin = joints[i].transform.position.z - 1;


            }
            else if (spawnAxis[i].z < 0)
            {
                joints[i].GetComponent<LeanConstrainLocalPosition>().ZMax = joints[i].transform.position.z + 1;
            }
            joints[i].SetActive(false);
        }
        for(int i = 0; i < wires.Length; i++)
        {
            wires[i].SetActive(false);
        }
        wires[0].SetActive(true);
        joints[0].SetActive(true);
        componentTerminals[0].SetActive(true);
        //joints[1].SetActive(true);
        Battery.SetActive(false);
        leftMotor.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NextWire()
    {
        if(jointIndex%2==0 && jointIndex< joints.Length)
        {
            wires[jointIndex / 2].gameObject.SetActive(true);
        }
    }
    public void NextJoint()
    {
        if (jointIndex < joints.Length)
        {
            componentTerminals[jointIndex].gameObject.SetActive(true);
            joints[jointIndex].gameObject.SetActive(true);
        }
        else {
            Debug.Log("==============Done=================");
            IntroBanner.stage=IntroBanner.stage+1;
        }
    }
    public void DisableTerminal()
    {
        componentTerminals[jointIndex].gameObject.SetActive(false);
    }
    public void NextObject()
    {
        if (jointIndex == 4)
        {
            leftMotor.SetActive(true);

        }
        if(jointIndex == 8)
        {
            Battery.SetActive(true);
        }
    }
}
