using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriverButton : MonoBehaviour
{
    [SerializeField] GameObject wheelRight;
    [SerializeField] GameObject wheelLeft;
    [SerializeField] GameObject[] mDButtons;
    [SerializeField] GameObject[] mDLight;
    [SerializeField] bool clockwise=true;
    [SerializeField] bool Right;
    Vector3[] mDButtonsPosition;

    // Start is called before the first frame update
    void Start()
    {
        wheelLeft.SetActive(false);
        wheelRight.SetActive(false);
        mDButtonsPosition = new Vector3[mDButtons.Length];
        for(int i = 0; i < mDButtons.Length; i++)
        {
            mDButtonsPosition[i] = mDButtons[i].transform.position;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void  OnMouseDown()
    {
        if (Input.GetKey("mouse 0") && MotorDriverCircut.jointIndex>11 )
        {
            ResetMdButtonPosition();
            if (Right == false)
            {
                if (clockwise == true)
                {
                    wheelLeft.GetComponent<DcMotorRunning>().clockwise = 1;
                    BttonClicked(2);
                }
                else
                {
                    wheelLeft.GetComponent<DcMotorRunning>().clockwise = -1;
                    BttonClicked(3);
                }
                wheelLeft.SetActive(true);
                wheelRight.SetActive(false);
            }
            else
            {
                if (clockwise == true)
                {
                    wheelRight.GetComponent<DcMotorRunning>().clockwise = 1;
                    BttonClicked(0);
                }
                else
                {
                    wheelRight.GetComponent<DcMotorRunning>().clockwise = -1;
                    BttonClicked(1);
                }
                wheelLeft.SetActive(false);
                wheelRight.SetActive(true);
            }
           
        }
    }
    void ResetMdButtonPosition()
    {
        for(int i = 0; i < mDButtonsPosition.Length; i++)
        {
            mDButtons[i].transform.position = mDButtonsPosition[i];
        }
    }
    void BttonClicked(int i)
    {
        
        mDButtons[i].transform.position = new Vector3(mDButtons[i].transform.position.x , mDButtons[i].transform.position.y-0.003f, mDButtons[i].transform.position.z );
    }
}
