using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Unity;
using UnityEngine.Events;

public class IntroBanner : MonoBehaviour
{
    // Start is called before the first frame update
    public string expirementName;
    public string[] introductionHeading;
    public string[] introductionDescription;
    GameObject variableForPrefab;
    GameObject introBanner;
    Button annotationButton;
    Button sideCanvasButton;
    Button run;
    GameObject[] cmpButton;
    int index = 0;
    public static int stage = 0;
    public int numberOfComponents;
    public GameObject[] componentsPrefabs;
    public string[] componentNames;
    bool annotationFlag = false;

    //public GameObject[] 
    public int threeDimViewerCompNum = -1;
    public GameObject[] threeDimViewerAssets;
    public int[] threeDimGroups;
    public string[] threeDimViewerHeading;
    public string[] threeDimViewerDescription;
    int threeDimIndex = 0;

    

    public GameObject mainModelPrefab;
    //public bool isInterfacingCanvas;
    //public GameObject interfacingCanvas;
    public bool isUnDo;
    public UnityEvent unDoEvent;
    Button unDoButton;
    public bool isInterfacingCanvas = false;
    public GameObject interfacingCanvas;
    public bool isAndroidCanvas = false;
    public GameObject arduinoCanvas;
    // Start is called before the first frame update
    void Start()
    {
        variableForPrefab = (GameObject)Resources.Load("WorldSpace/Prefabs/StartBanner", typeof(GameObject));
        Instantiate(variableForPrefab);
        introBanner = GameObject.Find("StartBanner(Clone)");

        Button btn = introBanner.transform.GetChild(0).GetChild(1).GetChild(3).GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        cmpButton = new GameObject[numberOfComponents];

        annotationButton = introBanner.transform.GetChild(0).GetChild(1).GetChild(5).GetComponent<Button>();
        annotationButton.onClick.AddListener(AnnotationOnClick);

        run = introBanner.transform.GetChild(0).GetChild(1).GetChild(7).GetComponent<Button>();
        run.onClick.AddListener(RunOnClick);

        unDoButton = introBanner.transform.GetChild(0).GetChild(1).GetChild(8).GetComponent<Button>();
        unDoButton.onClick.AddListener(UnDo);

        sideCanvasButton = introBanner.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Button>();

        //introBanner.transform.GetChild(0).GetChild(0).GetComponentInChildren<TextMeshPro>().text = "MDDC";
        introBanner.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = expirementName;
        introBanner.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().text = introductionHeading[index];
        introBanner.transform.GetChild(0).GetChild(1).GetChild(2).GetComponent<TextMeshProUGUI>().text = introductionDescription[index];
        stage = 1;
        introBanner.transform.GetChild(0).GetChild(1).GetChild(5).gameObject.SetActive(false);
        introBanner.transform.GetChild(0).GetChild(1).GetChild(7).gameObject.SetActive(false);
        mainModelPrefab.SetActive(false);
        introBanner.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(false);
        introBanner.transform.GetChild(0).GetChild(0).GetChild(8).gameObject.SetActive(false);

    }

    void TaskOnClick()
    {
        if (stage == 1)
        {
            index++;
            Debug.Log(index);
            if (index < introductionDescription.Length)
            {
                introBanner.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().text = introductionHeading[index];
                introBanner.transform.GetChild(0).GetChild(1).GetChild(2).GetComponent<TextMeshProUGUI>().text = introductionDescription[index];
            }
            else
            {
                stage++;
                
                introBanner.transform.GetChild(0).GetChild(1).GetChild(1).gameObject.SetActive(false);
                introBanner.transform.GetChild(0).GetChild(1).GetChild(2).gameObject.SetActive(false);
                if (numberOfComponents<1)
                {
                    TaskOnClick();
                    return;
                }
                
                introBanner.transform.GetChild(0).GetChild(1).GetChild(5).gameObject.SetActive(true);
                
                //Instantiate((GameObject)Resources.Load("WorldSpace/Prefabs/ComponentButton", typeof(GameObject)), new Vector3(0,0,0), Quaternion.identity);
                int j = 1, k = 1;
                for (int i = 0; i < numberOfComponents; i++)
                {
                    if (i % 3 == 0 && i != 0)
                    {
                        j = 1;
                        k++;
                    }
                    GameObject reference = introBanner.transform.GetChild(0).GetChild(1).GetChild(4).gameObject;
                    cmpButton[i] = Instantiate((GameObject)Resources.Load("WorldSpace/Prefabs/ComponentButton", typeof(GameObject)), reference.transform.position, Quaternion.identity, introBanner.transform.GetChild(0).GetChild(1).GetChild(6));
                    //cmpButton[i].transform.name = cmpButton[i].transform.name + i;
                    //cmpButton[i].GetComponent<Button>().onClick.AddListener(delegate() { ComponentOnClick(i); });
                    Button tempButton = cmpButton[i].GetComponent<Button>();
                    int tempInt = i;

                    tempButton.onClick.AddListener(() => ButtonClicked(tempInt));

                    //cmpButton.transform.localPosition = new Vector3(reference.transform.position.x + ((j * (2160 / 3)) - (2160 / 6)), reference.transform.position.y - ((k * (1080 / 3)) - (1080 / 6)), 0);
                    cmpButton[i].transform.localPosition = new Vector3(reference.transform.localPosition.x + ((j * (2160 / 3)) - (2160 / 6)), reference.transform.localPosition.y - ((k * (1080 / 3)) - (1080 / 6)), 0);
                    cmpButton[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = componentNames[i];
                    j++;

                }
                if(numberOfComponents>0)
                    ButtonClicked(0);
                
                
            }
           

        }
        else if (stage == 2)
        {
            
            mainModelPrefab.SetActive(true);
            ResetComponents();
            //introBanner.transform.GetChild(0).GetChild(1).GetChild(3).gameObject.SetActive(false);
            introBanner.transform.GetChild(0).GetChild(1).GetChild(6).gameObject.SetActive(false);
            introBanner.transform.GetChild(0).GetChild(1).GetChild(5).gameObject.SetActive(false);
            
            if (isUnDo)
            {
                introBanner.transform.GetChild(0).GetChild(0).GetChild(8).gameObject.SetActive(true);
            }
            if (isInterfacingCanvas)
            {
                 
                GameObject interfacinfCanvasTemp = interfacingCanvas;
                interfacinfCanvasTemp.transform.GetChild(0).parent = introBanner.transform.GetChild(0).GetChild(2);

            }

        }
        else if (stage == 3)
        {
            GameObject arduinoCanvasTemp = arduinoCanvas;
            //arduinoCanvasTemp.transform.parent = introBanner.transform.GetChild(0).GetChild(2);
            if (isAndroidCanvas)
            {
                introBanner.transform.GetChild(0).GetChild(1).GetChild(7).gameObject.SetActive(true);
                for (int i = 0; i <= arduinoCanvas.transform.childCount; i++)
                {
                    Debug.Log(arduinoCanvasTemp.transform.GetChild(0).name);
                    arduinoCanvasTemp.transform.GetChild(0).parent = introBanner.transform.GetChild(0).GetChild(2);
                }
            }
            if (isInterfacingCanvas)
            {
                GameObject interfacinfCanvasTemp=interfacingCanvas;
                interfacinfCanvasTemp.transform.GetChild(0).parent= introBanner.transform.GetChild(0).GetChild(2);



            }
            

        }
        Debug.Log(stage);





    }
    void ButtonClicked(int buttonNo)
    {
        ResetComponents();
        Debug.Log("Button clicked = " + buttonNo);
        componentsPrefabs[buttonNo].SetActive(true);  
        Debug.Log(threeDimViewerCompNum + " - " + buttonNo);
        if (threeDimViewerCompNum == buttonNo)
        {
            introBanner.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(true);
            sideCanvasButton.onClick.AddListener(ThreeDimViewerOnClick);
            introBanner.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = componentNames[buttonNo];
            introBanner.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = threeDimViewerHeading[threeDimIndex];
            introBanner.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().text = threeDimViewerDescription[threeDimIndex];
            threeDimIndex++;
        }
        else
        {
            introBanner.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = componentNames[buttonNo];
            introBanner.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = "";
            introBanner.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().text = "";
            threeDimIndex = 0;
        }
    }
    void ThreeDimViewerOnClick()
    {
        if (threeDimIndex < threeDimViewerDescription.Length)
        {
            introBanner.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = threeDimViewerHeading[threeDimIndex];
            introBanner.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().text = threeDimViewerDescription[threeDimIndex];
            ResetThreeDimComponent();
            int sum = 0;
            for(int i = 0; i < threeDimIndex-1; i++)
            {
                sum = sum + threeDimGroups[i];
            }
            for(int i = sum; i < sum + threeDimGroups[threeDimIndex-1]; i++)
            {
                threeDimViewerAssets[i].SetActive(true);
            }
            threeDimIndex++;

        }
        else
        {
            threeDimIndex = 0;
            if (threeDimViewerCompNum + 1 < cmpButton.Length)
                ButtonClicked(threeDimViewerCompNum + 1);
            else
                ButtonClicked(0);
        }

        
    }
    private void ResetComponents()
    {
        for (int i = 0; i < componentsPrefabs.Length; i++)
        {
            componentsPrefabs[i].SetActive(false);
        }
    }
    void ComponentOnClick(int componentNum)
    {
        for(int i = 0; i < componentsPrefabs.Length; i++)
        {
            componentsPrefabs[i].SetActive(false);
        }
        Debug.Log(componentNum);
        componentsPrefabs[componentNum].SetActive(true);
        

    }
    void AnnotationOnClick()
    {
        for(int i = 0; i < componentsPrefabs.Length; i++)
        {
            if (componentsPrefabs[i].active == true)
            {
                if (componentsPrefabs[i].transform.Find("Labels") != null )
                {
                    if (!annotationFlag)
                    {
                        componentsPrefabs[i].transform.Find("Labels").gameObject.SetActive(true);
                        annotationFlag = true;
                    }
                    else
                    {
                        componentsPrefabs[i].transform.Find("Labels").gameObject.SetActive(false);
                        annotationFlag = false;
                    }
                    
                    
                }
                
                if (componentsPrefabs[i].transform.Find("Board Labels") != null)
                {
                    if (!annotationFlag)
                    {
                        componentsPrefabs[i].transform.Find("Board Labels").gameObject.SetActive(true);
                        annotationFlag = true;
                    }
                    else
                    {
                        componentsPrefabs[i].transform.Find("Board Labels").gameObject.SetActive(false);
                        annotationFlag = false;
                    }
                }
            }
        }

    }
    private void ResetThreeDimComponent()
    {
        for(int i = 0; i < threeDimViewerAssets.Length; i++)
        {
            threeDimViewerAssets[i].SetActive(false);
        }
    }
    private void RunOnClick()
    {

    }
    private void UnDo()
    {
        unDoEvent.Invoke();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
