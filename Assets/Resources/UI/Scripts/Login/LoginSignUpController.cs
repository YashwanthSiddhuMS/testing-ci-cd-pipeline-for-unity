using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginSignUpController : MonoBehaviour
{
    // SignUp Text Fields
    [Header("SignUp fields")]
    [SerializeField]
    TMP_InputField firstNameInput;
    [SerializeField]
    TMP_InputField lastNameInput;
    [SerializeField]
    TMP_InputField dobInput;
    [SerializeField]
    TMP_InputField emailId;
    [SerializeField]
    TMP_InputField password;
    [SerializeField]
    TMP_InputField confirmPassword;
    [SerializeField]
    TMP_InputField phoneNum;
    [SerializeField]
    TMP_InputField city;
    [SerializeField]
    TMP_InputField state;
    [SerializeField]
    TMP_InputField country;
    [SerializeField]
    TMP_Text signUpReturnMessage;
    [SerializeField]
    TMP_Dropdown City;
    [SerializeField]
    TMP_Dropdown State;
    [SerializeField]
    TMP_Dropdown Country;
    [SerializeField]
    TMP_InputField ZipCode;


    // Login Text Fields
    [Header("Login fields")]
    [SerializeField]
    TMP_InputField loginEmailId;
    [SerializeField]
    TMP_InputField loginPassword;
    [SerializeField]
    TMP_Text loginReturnMessage;

    //Forgot Password Fields
    [Header("Forgot Password fields")]
    [SerializeField]
    TMP_InputField otpEmailId;
    [SerializeField]
    TMP_Text otpReturnMessage;

    //Change Password Fields
    [Header("Change Password fields")]
    [SerializeField]
    TMP_InputField[] otp;
    [SerializeField]
    TMP_Text otpTimer;
    [SerializeField]
    TMP_InputField updatePassword;
    [SerializeField]
    TMP_InputField updateConfirmPassword;
    [SerializeField]
    TMP_Text updateReturnMessage;

    // Scene Panel
    [Header("Screen Panels")]
    [SerializeField]
    GameObject entryScene_Panel;
    [SerializeField]
    GameObject loginScreen_Panel;
    [SerializeField]
    GameObject signUpScreen_Step_1_Panel;
    [SerializeField]
    GameObject signUpScreen_Step_2_Panel;
    [SerializeField]
    GameObject ForgotPassword_Panel;
    [SerializeField]
    GameObject OTPVerified_Panel;
    [SerializeField]
    GameObject ChangePassword_Panel;
    [SerializeField]
    GameObject PasswordChangeUpdated_Panel;

    // Resend Timing
    [Header("Resend timing")]
    public float resendTimeLimit = 30;
    float timeRemaining;
    bool timerRunning = false;

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void Update()
    {
        // Resend Timer
        if (timerRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                // Timer Completed
                timeRemaining = 0;
                timerRunning = false;

                // Set interactable of the Resend OTP button to true till the time completes
                otp[0].transform.parent.parent.GetChild(1).GetComponent<Button>().interactable = true;
            }

            // Timer Display
            otpTimer.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(timeRemaining / 60), Mathf.FloorToInt(timeRemaining % 60));
        }

        // BackButton Functionality in Mobile
        if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if(entryScene_Panel.activeInHierarchy)
                {
                    Application.Quit();
                }
                else if(loginScreen_Panel.activeInHierarchy)
                {
                    GoToEntry();
                }
                else if (signUpScreen_Step_1_Panel.activeInHierarchy)
                {
                    GoToEntry();
                }
                else if (signUpScreen_Step_2_Panel.activeInHierarchy)
                {
                    GoToSignUp_Step_1();
                }
                else if (ForgotPassword_Panel.activeInHierarchy)
                {
                    GoToLoginFromForgotPassword();
                }
                else if (ChangePassword_Panel.activeInHierarchy)
                {
                    GoToForgetPasswordFromUpdatepassword();
                }
            }
        }
    }

    public void CheckDateFormat()
    {
        // DOB "/" AutoFill
        if (Regex.IsMatch(dobInput.text, @"^[0-9][0-9]$") || Regex.IsMatch(dobInput.text, @"^[0-9][0-9]\/[0-9][0-9]$"))
        {
            dobInput.text = dobInput.text + "/";
            dobInput.MoveTextEnd(false);
        }
    }

    public void GoToEntry()
    {
        entryScene_Panel.SetActive(true);
        loginScreen_Panel.SetActive(false);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);

        // Reset SignUp Fields
        firstNameInput.text = "";
        lastNameInput.text = "";
        dobInput.text="";
        emailId.text = "";
        password.text = "";
        confirmPassword.text = "";
        phoneNum.text = "";
        city.text = "";
        state.text = "";
        country.text = "";
        signUpReturnMessage.text = "";
        // Reset Login Fields
        loginEmailId.text = "";
        loginPassword.text = "";
        loginReturnMessage.text = "";
        // Forget Password Fields
        otpEmailId.text = "";
        otpReturnMessage.text = "";
        // Update Password Fields
        foreach(TMP_InputField otpDigit in otp) { otpDigit.text = ""; }
        updatePassword.text = "";
        updateConfirmPassword.text = "";
        updateReturnMessage.text = "";
    }

    public void GoToLogin()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(true);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);
    }

    public void GoToSignUp_Step_1()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(false);
        signUpScreen_Step_1_Panel.SetActive(true);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);
    }

    public void GoToSignUp_Step_2()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(false);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(true);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);

        signUpReturnMessage.text = "";
    }

    public void GoToLoginAfterSignUp()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(true);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);

        // Reset SignUp Fields
        firstNameInput.text = "";
        lastNameInput.text = "";
        dobInput.text = "";
        emailId.text = "";
        password.text = "";
        confirmPassword.text = "";
        phoneNum.text = "";
        city.text = "";
        state.text = "";
        country.text = "";
        signUpReturnMessage.text = "";
    }

    public void GoToForgotPassword()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(false);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(true);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);

        // Reset Login Fields
        loginEmailId.text = "";
        loginPassword.text = "";
        loginReturnMessage.text = "";
    }

    public void GoToLoginFromForgotPassword()
    {
        entryScene_Panel.SetActive(false);
        loginScreen_Panel.SetActive(true);
        signUpScreen_Step_1_Panel.SetActive(false);
        signUpScreen_Step_2_Panel.SetActive(false);
        ForgotPassword_Panel.SetActive(false);
        OTPVerified_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(false);

        // Reset SignUp Fields
        firstNameInput.text = "";
        lastNameInput.text = "";
        dobInput.text = "";
        emailId.text = "";
        password.text = "";
        confirmPassword.text = "";
        phoneNum.text = "";
        city.text = "";
        state.text = "";
        country.text = "";
        signUpReturnMessage.text = "";
        // Reset Login Fields
        loginEmailId.text = "";
        loginPassword.text = "";
        loginReturnMessage.text = "";
        // Forget Password Fields
        otpEmailId.text = "";
        otpReturnMessage.text = "";
        // Update Password Fields
        foreach (TMP_InputField otpDigit in otp) { otpDigit.text = ""; }
        updatePassword.text = "";
        updateConfirmPassword.text = "";
        updateReturnMessage.text = "";
    }

    public void GoToUpdatePassword()
    {
        // Email OTP Fields
        otpReturnMessage.text = "";
        // Update Password Fields
        foreach (TMP_InputField otpDigit in otp) { otpDigit.text = ""; }
        updatePassword.text = "";
        updateConfirmPassword.text = "";
        updateReturnMessage.text = "";

        ForgotPassword_Panel.SetActive(false);
        ChangePassword_Panel.SetActive(true);
    }

    public void GoToForgetPasswordFromUpdatepassword()
    {
        // Forget Password Fields
        otpEmailId.text = "";
        otpReturnMessage.text = "";
        // Update Password Fields
        foreach (TMP_InputField otpDigit in otp) { otpDigit.text = ""; }
        updatePassword.text = "";
        updateConfirmPassword.text = "";
        updateReturnMessage.text = "";

        ForgotPassword_Panel.SetActive(true);
        ChangePassword_Panel.SetActive(false);

        ResendOTPFunction(true);
        PasswordFieldEnablerDisabler(false);
    }

    public void GoToPaswordUpdated()
    {
        // Forget Password Fields
        otpEmailId.text = "";
        otpReturnMessage.text = "";
        // Update Password Fields
        foreach (TMP_InputField otpDigit in otp) { otpDigit.text = ""; }
        updatePassword.text = "";
        updateConfirmPassword.text = "";
        updateReturnMessage.text = "";

        ChangePassword_Panel.SetActive(false);
        PasswordChangeUpdated_Panel.SetActive(true);
    }

    public void ShowPassword(TMP_InputField passwordField)
    {
        passwordField.contentType = TMP_InputField.ContentType.Standard;
        passwordField.ForceLabelUpdate();

        passwordField.ActivateInputField();
    }

    public void HidePassword(TMP_InputField passwordField)
    {
        passwordField.contentType = TMP_InputField.ContentType.Password;
        passwordField.ForceLabelUpdate();

        passwordField.ActivateInputField();
    }

    public void PasswordFieldEnablerDisabler(bool enableField)
    {
        if (enableField)
        {
            // New Password Field
            // Setting blue color for New Password text
            updatePassword.transform.parent.gameObject.GetComponent<Image>().color = new Color32(56, 166, 220, 255);
            // Setting interactable of New Password InputFields to true
            updatePassword.interactable = true;
            // Setting blue color for Enter New Password placeholder text
            updatePassword.placeholder.color = new Color32(56, 166, 220, 255);
            // Setting show New Password button interactable to true
            updatePassword.transform.parent.GetChild(1).GetComponent<Button>().interactable = true;

            // Confirm New Password Field
            // Setting blue color for New Password text
            updateConfirmPassword.transform.parent.gameObject.GetComponent<Image>().color = new Color32(56, 166, 220, 255);
            // Setting interactable of New Password InputFields to true
            updateConfirmPassword.interactable = true;
            // Setting blue color for Enter New Password placeholder text
            updateConfirmPassword.placeholder.color = new Color32(56, 166, 220, 255);
            // Setting show New Password button interactable to true
            updateConfirmPassword.transform.parent.GetChild(1).GetComponent<Button>().interactable = true;
        }
        else
        {
            // New Password Field
            // Setting blue color for New Password text
            updatePassword.transform.parent.gameObject.GetComponent<Image>().color = new Color32(143, 146, 161, 255);
            // Setting interactable of New Password InputFields to true
            updatePassword.interactable = false;
            // Setting blue color for Enter New Password placeholder text
            updatePassword.placeholder.color = new Color32(143, 146, 161, 128);
            // Setting show New Password button interactable to true
            updatePassword.transform.parent.GetChild(1).GetComponent<Button>().interactable = false;

            // Confirm New Password Field
            // Setting blue color for New Password text
            updateConfirmPassword.transform.parent.gameObject.GetComponent<Image>().color = new Color32(143, 146, 161, 255);
            // Setting interactable of New Password InputFields to true
            updateConfirmPassword.interactable = false;
            // Setting blue color for Enter New Password placeholder text
            updateConfirmPassword.placeholder.color = new Color32(143, 146, 161, 128);
            // Setting show New Password button interactable to true
            updateConfirmPassword.transform.parent.GetChild(1).GetComponent<Button>().interactable = false;
        }
    }
    
    public void ResendOTPFunction(bool resetTimer)
    {    
        if(!resetTimer)
        {
            // Set resend message time limit
            timeRemaining = resendTimeLimit;
            // Start the timer
            timerRunning = true;

            // Set interactable of the Resend OTP button to false till the time completes
            otp[0].transform.parent.parent.GetChild(1).GetComponent<Button>().interactable = false;
        }
        else
        {
            // Reset Timer
            timeRemaining = 0;
            timerRunning = false;
            otpTimer.text = string.Format("{0:00}:{1:00}", 0, 0);

            // Set interactable of the Resend OTP button to true till the time completes
            otp[0].transform.parent.parent.GetChild(1).GetComponent<Button>().interactable = true;
        }
    }
}
