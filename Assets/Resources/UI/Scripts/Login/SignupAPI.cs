﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignUpData
{
    public string firstname;
    public string lastname;
    public string email;
    public string password;
    public string standard;
    public string institute;
    public string branch;
    public string contact;
    public string city;
    public string state;
    public string country;
    public string dob;
    public string profession;
    public bool scholarship;
    public string @event;

}

public class SignupAPI : MonoBehaviour
{
    [SerializeField]
    TMP_InputField firstNameInput;
    [SerializeField]
    TMP_InputField lastNameInput;
    [SerializeField]
    TMP_InputField dobInput;
    [SerializeField]
    TMP_InputField emailIdInput;
    [SerializeField]
    TMP_InputField password;
    [SerializeField]
    TMP_InputField confirmPassword;
    [SerializeField]
    TMP_InputField phoneNum;
    [SerializeField]
    TMP_InputField city;
    [SerializeField]
    TMP_InputField state;
    [SerializeField]
    TMP_InputField country;
    [SerializeField]
    TMP_Text returnMessage;
    [SerializeField]
    TMP_Dropdown City;
    [SerializeField]
    TMP_Dropdown State;
    [SerializeField]
    TMP_Dropdown Country;
    [SerializeField]
    TMP_InputField ZipCode;
    [SerializeField]
    TMP_Text returnMessageLogin;

    [SerializeField]
    GameObject loading_Panel;
    [SerializeField]
    GameObject loadingCircle;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public const string MatchDOBPattern = @"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";

    string signUpurl = "https://portal-api.holo-world.io/users/signup";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static bool validateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    public static bool validateDOB(string dob)
    {
        if (dob != null)
        {
            if (Regex.IsMatch(dob, MatchDOBPattern))
            {
                if (System.DateTime.Compare(System.DateTime.Now, System.DateTime.Parse(dob)) >= 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }

    public static bool validatePassword(string password)
    {
        if (password.Any(character => char.IsUpper(character)) &&
            password.Any(character => char.IsLower(character)) &&
            password.Any(character => char.IsDigit(character)) &&
            password.IndexOfAny("!@#$%^&*()".ToCharArray()) != -1)
            return true;
        else
            return false;
    }

    IEnumerator signUpCall(string firstname, string lastname, string dob, string email, string password, string phoneNum, string city, string state, string country)
    {
        var signUpData = new SignUpData();
        /*
        signUpData.firstname = firstname;
        signUpData.lastname = lastname;
        signUpData.email = email;
        signUpData.password = password;
        signUpData.standard = "";
        signUpData.institute = "";
        signUpData.branch = "";
        signUpData.contact = phoneNum;
        signUpData.city = city;
        signUpData.state = state;
        signUpData.country = country;
        signUpData.dob = "";
        signUpData.profession = "";
        signUpData.scholarship = false;
        signUpData.@event = "";
        */

        signUpData.firstname = firstname;
        signUpData.lastname = firstname;
        signUpData.email = email;
        signUpData.password = password;
        signUpData.standard = "";
        signUpData.institute = "";
        signUpData.branch = "";
        signUpData.contact = phoneNum;
        signUpData.city = city;
        signUpData.state = state;
        signUpData.country = country;
        signUpData.dob = dob;
        signUpData.profession = "";
        signUpData.scholarship = false;
        signUpData.@event = "";


        //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        //formData.Add(new MultipartFormFileSection("firstname", firstname));
        //formData.Add(new MultipartFormFileSection("lastname", lastname));
        //formData.Add(new MultipartFormFileSection("email", email));
        //formData.Add(new MultipartFormFileSection("password", password));
        //formData.Add(new MultipartFormFileSection("contact", phoneNum));
        //formData.Add(new MultipartFormFileSection("city", city));
        //formData.Add(new MultipartFormFileSection("state", state));
        //formData.Add(new MultipartFormFileSection("country", country));

        //WWWForm wwwf = new WWWForm();
        //wwwf.AddField("firstname", firstname);
        //wwwf.AddField("lastname", lastname);
        //wwwf.AddField("email", email);
        //wwwf.AddField("password", password);
        //wwwf.AddField("standard", "");
        //wwwf.AddField("institute", "");
        //wwwf.AddField("branch", "");
        //wwwf.AddField("contact", phoneNum);
        //wwwf.AddField("city", city);
        //wwwf.AddField("state", state);
        //wwwf.AddField("country", country);
        //wwwf.AddField("dob", country);
        //wwwf.AddField("profession", country);
        //wwwf.AddField("scholarship", country);
        //wwwf.AddField("event", country);


        string json = JsonUtility.ToJson(signUpData);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

        //UnityWebRequest req = UnityWebRequest.Post(signUpurl, wwwf);

        UnityWebRequest req = new UnityWebRequest(signUpurl, "POST");
        req.uploadHandler = new UploadHandlerRaw(jsonToSend);
        req.downloadHandler = new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");

        yield return req.SendWebRequest();

        if (req.isNetworkError)
        {
            Debug.Log("Error while sending: " + req.error);
            loadingCircle.SetActive(false);
            returnMessage.text = "Error while sending: " + req.error;
        }
        else
        {
            Debug.Log("Received: " + req.downloadHandler.text);
            loadingCircle.SetActive(false);
            returnMessage.text = "Received: " + req.downloadHandler.text;

            this.GetComponent<LoginSignUpController>().GoToLoginAfterSignUp();
            returnMessageLogin.text = "SignUp Successful. Kindly Login....";
        }
    }

    public void OnPressSignUp()
    {
        if (firstNameInput.text.Length < 3)
        {
            returnMessage.text = "Enter a valid Name";
        }
        else if (phoneNum.text.Length != 10)
        {
            returnMessage.text = "Enter a valid Phone Number";
        }
        else if (!validateDOB(dobInput.text))
        {
            returnMessage.text = "Enter a valid DOB";
        }
        else if (city.text.Length != 6)
        {
            returnMessage.text = "Enter a valid Pincode";
        }
        else if (!validateEmail(emailIdInput.text))
        {
            returnMessage.text = "Enter a valid Email Id";
        }
        else if (!validatePassword(password.text))
        {
            returnMessage.text = "Password should contain Atleast 1 uppercase, 1 lowercase, 1 special character, 1 number and length greater than 8";
        }
        else if (confirmPassword.text != password.text)
        {
            returnMessage.text = "Mismatch in Password";
        }
        else
        {
            loadingCircle.SetActive(true);
            StartCoroutine(signUpCall(firstNameInput.text, lastNameInput.text, dobInput.text, emailIdInput.text, password.text, phoneNum.text, city.text, state.text, country.text));
        }
    }
}
