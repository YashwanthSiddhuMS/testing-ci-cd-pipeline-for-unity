﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class resetPasswordData
{
    public string email;
    public string otp;
    public string password;
}

public class ResetPassword : MonoBehaviour
{
    [SerializeField]
    TMP_InputField email;
    [SerializeField]
    TMP_InputField[] OTP;
    [SerializeField]
    TMP_InputField newPassword;
    [SerializeField]
    TMP_InputField confirmNewPassword;

    [SerializeField]
    TMP_Text errorMessage;

    [SerializeField]
    GameObject loadingCircle;

    string url = "https://portal-api.holo-world.io/users/resetPassword";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static bool validatePassword(string password)
    {
        if (password.Any(character => char.IsUpper(character)) &&
            password.Any(character => char.IsLower(character)) &&
            password.Any(character => char.IsDigit(character)) &&
            password.IndexOfAny("!@#$%^&*()".ToCharArray()) != -1)
            return true;
        else
            return false;
    }

    IEnumerator callResetPassword(string email, string otp, string password)
    {
        var resetpass = new resetPasswordData();
        resetpass.email = email;
        resetpass.otp = otp;
        resetpass.password = password;

        string json = JsonUtility.ToJson(resetpass);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

        UnityWebRequest req = new UnityWebRequest(url, "PATCH");

        req.uploadHandler = new UploadHandlerRaw(jsonToSend);
        req.downloadHandler = new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");

        yield return req.SendWebRequest();

        if (req.isNetworkError)
        {
            loadingCircle.SetActive(false);
            errorMessage.text = "Network Error !";
            Debug.Log("Error While Sending: " + req.error);
        }
        else
        {
            Debug.Log("Received: " + req.downloadHandler.text);
            JSONNode resjson = JSON.Parse(req.downloadHandler.text);

            string msg = resjson[0][0]["msg"];
            Debug.Log(msg);

            if (msg == "User is not available")
            {
                loadingCircle.SetActive(false);
                errorMessage.text = msg;
                Debug.Log("test : User is not available");
            }
            else if (msg == "Reset Password Unsuccessfully")
            {
                loadingCircle.SetActive(false);
                errorMessage.text = "OTP wrong or expired";
                Debug.Log("test : otp wrong or expired");
            }
            else if (resjson[0]["msg"] == "Reset Password Successfully !")
            {
                loadingCircle.SetActive(false);
                errorMessage.text = resjson[0]["msg"];
                Debug.Log("test : Reset Password Successfully !");

                this.GetComponent<LoginSignUpController>().GoToPaswordUpdated();
            }
            else
            {
                loadingCircle.SetActive(false);
                errorMessage.text = msg;
            }
        }
    }

    public void OnPressReset()
    {
        if (!validatePassword(newPassword.text))
        {
            errorMessage.text = "Password should contain Atleast 1 uppercase, 1 lowercase, 1 special character, 1 number and length greater than 8";
        }
        else if (confirmNewPassword.text != newPassword.text)
        {
            errorMessage.text = "Mismatch in Password";
        }
        else
        {
            string otpText = "";
            foreach(TMP_InputField otpDigit in OTP) { otpText = otpText + otpDigit.text; }
            loadingCircle.SetActive(true);
            StartCoroutine(callResetPassword(email.text, otpText, newPassword.text));
        }
    }
}
