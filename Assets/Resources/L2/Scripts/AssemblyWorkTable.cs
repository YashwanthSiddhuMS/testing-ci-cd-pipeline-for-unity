﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class AssemblyWorkTable : MonoBehaviour
{

    public List<GameObject> _partPrefabs;
    public GameObject[] _partPrefabArray;
    public Vector3[] localScales;
    public Vector3[] localSpawnPositions;
    public string[] _partContent;

    public Material highLight;

    public GameObject SpawnPoint, BiloidModel , _mainModel;

    public GameObject InfoTextManager;
    public AssemblyManager assemblyManager;

    public static bool is_training;

    public int index;

    public string[] _Tags;

    private int flipIndex = 62;
    List<Transform> stack = new List<Transform>();

    public TextMeshProUGUI stepsCanvas;
    public string[] interfacingSteps;
    // Start is called before the first frame update
    void Start()
    {
        Utils.assemblyComplete = true;
        index = 0;
    }

    private void Update()
    {
        
        

    }


    public void SetGOList(List<GameObject> _gameObjects ,string[] tags)
    {
        _partPrefabs = _gameObjects;
        _Tags = tags;
        _partPrefabArray = _partPrefabs.ToArray();

        for(int i = 0; i < _partPrefabArray.Length; i++)
        {
            _partPrefabArray[i].SetActive(false);
            Transform tempstack = _gameObjects[i].transform;
            stack.Add(tempstack);
            Debug.Log(tempstack.tag +" : "+ tempstack.position);

        }


        SpawnObjects(Utils.GameMode.Training);
        Invoke("SpawnNextIndexOf", 0);
        //stepsCanvas.text = "sdsdsd";// interfacingSteps[0];

    }


    public void SpawnObjects(Utils.GameMode gameMode)
    {
        switch(gameMode)
        {
            case Utils.GameMode.Training:
                SpawnNextIndexOf();
                break;

            case Utils.GameMode.Practice:
                //SpawnEverything(_partPrefabArray);
                break;
        }
    }



    public void SpawnNextIndexOf()
    {
        
        if(_partPrefabArray!=null)
        {
            
            if (index < _partPrefabArray.Length)
            {
                stepsCanvas.text = interfacingSteps[index];
                for (int i = 0; i < _partPrefabArray.Length; i++)
                {
                    if (_partPrefabArray[i] != null)
                    {
                        
                        _partPrefabArray[i].SetActive(false);
                        _partPrefabArray[index].GetComponent<Lean.Touch.LeanDragTranslate>().enabled = false;

                    }
                }
                if (_partPrefabArray[index] != null)
                {
                    _partPrefabArray[index].transform.localScale = localScales[index];
                    _partPrefabArray[index].SetActive(true);
                    _partPrefabArray[index].GetComponent<Lean.Touch.LeanDragTranslate>().enabled = true;
                    _partPrefabArray[index].AddComponent<Lean.Common.LeanConstrainLocalPosition>();
                    _partPrefabArray[index].AddComponent<CloneMovement>();
                    _partPrefabArray[index].transform.localScale = VectorMult(_partPrefabArray[index].transform.localScale , _mainModel.transform.localScale);
                    //_partPrefabArray[index].AddComponent<Lean.Touch.LeanPinchScale>();
                    //_partPrefabArray[index].AddComponent<LineRenderer>();
                    Debug.Log(_partPrefabArray[index].tag);
                }

                if (is_training)
                {
                    
                    hint();
                }
                else if (!is_training)
                {
                    StartCoroutine(practice());
                }
            }
            else
            {
                Utils.assemblyComplete = true;
                //SceneManager.LoadScene("ScrewAssembly");
                Debug.Log("No More Parts after this!");
                //InfoTextManager.SendMessage("DisplayTextForSecondsCaller", "Great! Let's now see how the motors work. Select a motor by clicking on it and use the slider to rotate it");
            }
        }
    }


    public void ReDo()
    {
        index = index - 1;
        //_partPrefabArray[index].transform.position = stack[index].transform.position;
        _partPrefabArray[index].transform.position= new Vector3(_partPrefabArray[index].transform.position.x + (assemblyManager.localSpawns[index].x / 10), _partPrefabArray[index].transform.position.y + (assemblyManager.localSpawns[index].y / 10), _partPrefabArray[index].transform.position.z + (assemblyManager.localSpawns[index].z / 10));
        Debug.Log("original  : " + assemblyManager.stackObjs[index].tag);

        DisableMeshrenderer(index);
        DisableMeshrenderer(index + 1);

        //assemblyManager.stackObjs[index+1].GetComponent<MeshRenderer>().enabled = false;
        SpawnNextIndexOf();

    }

    public void DisplayText(int index)
    {
        //InfoTextManager.SendMessage("DisplayTextForSecondsCaller", _partContent[index]);
    }
    
    public void flipRobot(int index)
    {
        if (index == flipIndex)
        {
            BiloidModel.transform.rotation = Quaternion.Euler(0, 0, 180f);
        }

    }

    public void hint()
    {
        Renderer[] _meshRenderers = GameObject.FindGameObjectWithTag(_Tags[index]).GetComponentsInChildren<Renderer>();
        //Debug.Log("Len of mesh renderers : "+ _meshRenderers.Length);
        for (int i = 0; i < _meshRenderers.Length; i++)
        {
            _meshRenderers[i].enabled = true;
            _meshRenderers[i].material = highLight;                                                                                                                  
        }
    }
     IEnumerator practice()

     {
        yield return new WaitForSeconds(4f);
        hint();

     }

    public void trainingMode()
    {
        is_training = true;
    }

    public void practiceMode()
    {
        is_training = false;
    }
    Vector3 VectorMult(Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
    }
    void DisableMeshrenderer(int index)
    {
        try
        {
            assemblyManager.stackObjs[index].GetComponent<MeshRenderer>().enabled = false;
            for (int i = 0; i < assemblyManager.stackObjs[index].transform.childCount; i++)
            {
                assemblyManager.stackObjs[index].transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
            }
        }
        catch
        {
            for (int i = 0; i < assemblyManager.stackObjs[index].transform.childCount; i++)
            {
                assemblyManager.stackObjs[index].transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }

}
