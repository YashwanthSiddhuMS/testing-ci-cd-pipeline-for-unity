﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
public class AssemblyManager : MonoBehaviour
{


    public GameObject _mainModel;
    
    public string[] Tags;

    public List<GameObject> _partPrefabs;

    public MeshRenderer[] _mainModelMeshRenderers;

    public GameObject PartHolder;
    public Vector3[] localSpawnPositions;
    public Vector3[] localSpawns;
    //private Vector3 scaleChange;
    public List<GameObject> stackObjs=new List<GameObject>();

    //public MenuBuilder menuBuilder;
    public AssemblyWorkTable _assemblyWorkTable;
    //public GameObject[] originalGameObjt;

   


   

    // Start is called before the first frame update
    void Start()
    {

        for(int i = 0; i<Tags.Length; i++)
        {
            GameObject[] gameObjectsWithTag; 
            gameObjectsWithTag = GameObject.FindGameObjectsWithTag(Tags[i]);
            if(gameObjectsWithTag.Length>0)
            {
                for(int j = 0; j<gameObjectsWithTag.Length; j++)
                {
                    //PartHolder.transform.position = localSpawnPositions[i];
                    PartHolder.transform.position = new Vector3(gameObjectsWithTag[j].transform.position.x+(localSpawns[i].x/10), gameObjectsWithTag[j].transform.position.y+( localSpawns[i].y/10), gameObjectsWithTag[j].transform.position.z+(localSpawns[i].z/10));
                    GameObject gO;
                    
                    if (gameObjectsWithTag[j].tag == "AS_Battery")
                    {
                         gO = Instantiate(gameObjectsWithTag[j], PartHolder.transform.position, Quaternion.Euler( new Vector3(-90f, 0f, -90f)));
                    }
                    else
                    {
                         gO = Instantiate(gameObjectsWithTag[j], PartHolder.transform.position, gameObjectsWithTag[j].transform.rotation);
                    }
                    stackObjs.Add(gameObjectsWithTag[j]);
                    gO.transform.parent = _mainModel.transform;
                    Destroy(gO.GetComponent<OnMouseDownPivot>());
                    gO.AddComponent<OnDestroyPartPrefab>();
                    gO.AddComponent<DragNew>();

                    //gO.transform.localScale= new Vector3(1, 1, 1);
                    _partPrefabs.Add(gO);
                    gameObjectsWithTag[j].AddComponent<OnCollisionFromPart>();

                }
            }
        }

        foreach(GameObject gO in _partPrefabs)
        {
            MeshRenderer[] _meshRenderers = gO.GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < _meshRenderers.Length; i++)
            {
                _meshRenderers[i].enabled = true;
            }
        }

        if (_assemblyWorkTable != null)
        {
            _assemblyWorkTable.SetGOList(_partPrefabs, Tags);
        }
        Debug.Log("==========");
    }

    // Update is called once per frame
    void Update()
    {
        /*if (_partPrefabs.Count > 0)
        {
            for(int i = 0; i < _partPrefabs.Count; i++)
            {
                try
                {
                    _partPrefabs[i].transform.localScale = new Vector3(1f, 1f, 1f);
                }
                catch
                {
                   
                }
                
            }
        }*/
    }
}
