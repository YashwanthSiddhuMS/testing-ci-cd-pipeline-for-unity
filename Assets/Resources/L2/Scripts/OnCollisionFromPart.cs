﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionFromPart : MonoBehaviour
{

    private MeshRenderer[] _meshRenderers;

    private MeshRenderer[] otherRenderers;

    private GameObject screws;


    private void Start()
    {
        _meshRenderers = GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < _meshRenderers.Length; i++)
        {
            _meshRenderers[i].enabled = false;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == this.gameObject.tag)
        {
            _meshRenderers = GetComponentsInChildren<MeshRenderer>();
            otherRenderers = other.GetComponentsInChildren<MeshRenderer>();
            //Debug.Log("trigg");
            for (int i = 0; i < _meshRenderers.Length; i++)
            {
                _meshRenderers[i].enabled = true;
                _meshRenderers[i].material = otherRenderers[i].material;
            }
            //Destroy(other.gameObject);


            var temp = GameObject.Find("AssemblyWorkBench").GetComponent<AssemblyWorkTable>();
            //if (temp != null)
            //temp.SendMessage("SpawnNextIndexOf");

            temp.index += 1;
            temp.SpawnNextIndexOf();
            //temp.flipRobot(temp.index);
        }
    }

   
}
