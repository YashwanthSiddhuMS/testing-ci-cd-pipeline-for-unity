using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Common;
using System;

public class CloneMovement : MonoBehaviour
{
    public Vector3 startPos= new Vector3(0f,0f,0f);
    // Start is called before the first frame update
    //public AssemblyManager assemblyManager;
    GameObject[] originalAndcloneObjects;
    GameObject originalObjt;
    GameObject cloneObjtoriginal;
    Transform cloneObjt;
   
    Vector3[] positionsLineRenderer=new Vector3[2];
    GameObject pathLine;

    void Start()
    {
        //startPos = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Awake()
    {
        startPos = this.gameObject.transform.position;
        cloneObjt = this.gameObject.transform;
        //Debug.Log("In Clone Movement : " + startPos);
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().enabled = true;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().X = true;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().XMin = startPos.x;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().XMax = startPos.x;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().Y = true;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().YMin = startPos.y;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().YMax = startPos.y;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().Z = true;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().ZMin = startPos.z;
        cloneObjt.GetComponent<LeanConstrainLocalPosition>().ZMax = startPos.z;

        originalAndcloneObjects = GameObject.FindGameObjectsWithTag(this.gameObject.tag);
        //Debug.Log("Length : " + (GameObject.FindGameObjectsWithTag(this.gameObject.tag).Length));

        for(int i = 0; i < originalAndcloneObjects.Length; i++)
        {


            if (!originalAndcloneObjects[i].transform.name.Contains("Clone"))
            {
                originalObjt = originalAndcloneObjects[i];
               
            }
            else
            {
                cloneObjtoriginal= originalAndcloneObjects[i];
                
            }


        }
        pathLine = GameObject.FindGameObjectWithTag("AS_PathLine");
        positionsLineRenderer[0] = cloneObjtoriginal.transform.position;
        positionsLineRenderer[1] = originalObjt.transform.position;
        pathLine.GetComponent<LineRenderer>().SetPositions(positionsLineRenderer);
        pathLine.GetComponent<LineRenderer>().SetWidth(0.001f, 0.001f);
        if (originalObjt.transform.position != cloneObjtoriginal.transform.position)
        {
            //Debug.Log("dcfd: " + originalAndcloneObjects[i].transform.position + " cffe : " + this.gameObject.transform.position);
            //Debug.Log("x: " + originalObjt.transform.position.x + " - : " + cloneObjtoriginal.transform.position.x);
            //Debug.Log("y: " + originalObjt.transform.position.y + " - : " + cloneObjtoriginal.transform.position.y);
            //Debug.Log("z: " + originalObjt.transform.position.z + " - : " + cloneObjtoriginal.transform.position.z);

            if (!nearlyEqual(originalObjt.transform.position.x, cloneObjtoriginal.transform.position.x))
            {
                if (originalObjt.transform.position.x > cloneObjtoriginal.transform.position.x)
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().XMax = startPos.x + 5;
                }
                else
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().XMin = startPos.x - 5;

                }
            }
            else if (!nearlyEqual(originalObjt.transform.position.y, cloneObjtoriginal.transform.position.y))
            {
                if (originalObjt.transform.position.y > cloneObjtoriginal.transform.position.y)
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().YMax = startPos.y + 5;
           
                }
                else
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().YMin = startPos.y - 5;
                

                }
            }
            else if (!nearlyEqual(originalObjt.transform.position.z, cloneObjtoriginal.transform.position.z))
            {
                if (originalObjt.transform.position.z > cloneObjtoriginal.transform.position.z)
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().ZMax = startPos.z + 5;
                 
                }
                else
                {
                    this.gameObject.GetComponent<LeanConstrainLocalPosition>().ZMin = startPos.z - 5;
                   

                }
            }
            else
            {
                //Debug.Log("dvdvdvd");
            }
        }

    }
    public static bool nearlyEqual(float a, float b)
    {
        //int absA = (int)Math.Abs(a*10);
        //int absB = (int)Math.Abs(b*10);
        int absA = (int)(a * 100);
        int absB = (int)(b * 100 );
        //Debug.Log("aa : " + absA + " bb : " + absB);
        float diff = Math.Abs(a - b);

        if (absA==absB)
        { // shortcut, handles infinities
            return true;
        }
        return false;
       
    }
    private void Update()
    {

        positionsLineRenderer[0] = this.transform.position;
        positionsLineRenderer[1] = originalObjt.transform.position; ;
        pathLine.GetComponent<LineRenderer>().SetPositions(positionsLineRenderer);



    }

}
