﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoTabletController : MonoBehaviour
{

    public GameObject InfoTablet;
    bool on;
    // Start is called before the first frame update
    void Start()
    {
        on = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            if(!on)
            {
                InfoTablet.SetActive(true);
                on = true;
            }
            else
            {
                InfoTablet.SetActive(false);
                on = false;
            }

          
        }
    }
}
