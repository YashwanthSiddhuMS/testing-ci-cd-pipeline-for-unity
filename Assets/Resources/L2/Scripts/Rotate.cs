﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public GameObject gO;

    [SerializeField]
    public float speed;

    public enum Axis
    {
        X,Y,Z
    };

    public Axis axis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(axis)
        {

            case Axis.X:
                gO.transform.Rotate(new Vector3(1, 0, 0), speed);
                break;

            case Axis.Y:
                gO.transform.Rotate(new Vector3(0, 1, 0), speed);
                break;

            case Axis.Z:
                gO.transform.Rotate(new Vector3(0, 0, 1), speed);
                break;
        }
    }
}
