﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoTextManager : MonoBehaviour
{


    public Text InfoText, Infotext1;

    public float seconds = 100f;

    // Start is called before the first frame update
    void Start()
    {
        string str = "Welcome! Today, we will learn about the different components of a humanoid robot, It's motors and sensors";
        string str2 = "Start by moving towards, and picking up the first component and placing it towards the transparent clone";
        StartCoroutine(DisplayTextForSeconds(str, seconds));
        StartCoroutine(wait());
        StartCoroutine(DisplayTextForSeconds(str2, seconds));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public IEnumerator DisplayTextForSeconds(string text, float seconds)
    {
        InfoText.text = text;
        Infotext1.text = text;
        yield return new WaitForSeconds(seconds);
        InfoText.text = "";
        Infotext1.text = "";
    }

    public void DisplayTextForSecondsCaller(string text)
    {
        StartCoroutine(DisplayTextForSeconds(text, seconds));
    }

    public void ChangeText(string text)
    {
        InfoText.text = text;
        Infotext1.text = text;
    }

    public IEnumerator wait()
    {
        yield return new WaitForSeconds(10);
    }
}
