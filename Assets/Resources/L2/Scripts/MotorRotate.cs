﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotorRotate : MonoBehaviour
{

    public GameObject _selectedObject;

    public Vector3 motorInitialRotation;

    public float prevSliderValue = 0;
    bool settingSliderFromCode;

    public Dictionary<GameObject, float> motorSliderValues;



    public InfoTextManager infoTextManager;
    Vector3 Axis = new Vector3();
    float axisValue;
    float _oldSliderValue;
    float time = 0f;
    public Slider _slider;
    //public enum Axis
    //{
    //    X, Y, Z
    //}

    // Start is called before the first frame update
    void Start()
    {
       
        _slider.gameObject.SetActive(false);
        motorSliderValues = new Dictionary<GameObject, float>();



    }

    // Update is called once per frame
    void Update()
    {
       
      


    }


    public void SetMotorToRotate(GameObject motor)
    {

        prevSliderValue = 0f;
       
        _selectedObject = motor;
         Axis = _selectedObject.transform.localRotation.eulerAngles;
        _slider.gameObject.SetActive(true);
       
        if (!motorSliderValues.ContainsKey(motor))
        {
            motorSliderValues.Add(motor, 0);
            _slider.value = 0;
        }
        else
        {
            settingSliderFromCode = true;
            _slider.value = motorSliderValues[_selectedObject];
          
        }

       
        //_slider.value = 0;







    }

    public void SetMotorInitialRotation(Vector3 motorRotation)
    {
        motorInitialRotation = motorRotation;
    }

    public void Rotate()
    {
        if(settingSliderFromCode)
        {
            settingSliderFromCode = false;
            return;

        }

        float sliderValue = _slider.value - prevSliderValue;

        if (motorSliderValues.ContainsKey(_selectedObject))
        {
            sliderValue = _slider.value - motorSliderValues[_selectedObject];
        }


        if (_selectedObject!=null)
        {

            if (_selectedObject.name.Contains("(X)"))
            {
                _selectedObject.transform.localRotation = Quaternion.Euler(Axis.x + sliderValue, Axis.y, Axis.z);

            }
            else if (_selectedObject.name.Contains("(Y)"))
            {
               _selectedObject.transform.localRotation = Quaternion.Euler(Axis.x , Axis.y + sliderValue, Axis.z);
            }
            else if (_selectedObject.name.Contains("(Z)"))
            {
                _selectedObject.transform.localRotation = Quaternion.Euler(Axis.x, Axis.y, Axis.z + sliderValue);
            }

           
            Axis = _selectedObject.transform.localEulerAngles;

            

           infoTextManager.SendMessage("ChangeText", "Rotating " + _selectedObject.name + " " + ((int)_slider.value).ToString() + " Deg");
           prevSliderValue =  _slider.value;

            if (motorSliderValues.ContainsKey(_selectedObject))
            {
                motorSliderValues[_selectedObject] = _slider.value;
            }







        }
    }
}
