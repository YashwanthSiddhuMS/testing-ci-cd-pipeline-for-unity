﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseDownPivot : MonoBehaviour
{


    Vector3 initialRotation;
    bool initialRotationStored;

    // Start is called before the first frame update
    void Start()
    {
        initialRotationStored = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (Utils.assemblyComplete)
        {
            if (!initialRotationStored)
            {
                initialRotation = gameObject.transform.parent.localEulerAngles;
                initialRotationStored = true;
            }

            GameObject.Find("InfoTextManager").SendMessage("ChangeText", "Selected: " + gameObject.transform.parent.name + ". Rotate it with the slider!");
            GameObject.Find("MotorRotator").SendMessage("SetMotorInitialRotation", initialRotation);
            GameObject.Find("MotorRotator").SendMessage("SetMotorToRotate", this.gameObject.transform.parent.gameObject);
        }
    }
}
